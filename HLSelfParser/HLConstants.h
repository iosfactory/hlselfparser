//
//  HLConstants.h
//  HLSelfParser
//
//  Created by Hervé HEURTAULT DE LAMMERVILLE on 7/14/13.
//  Copyright (c) 2013 Hervé Heurtault de Lammerville. All rights reserved.
//

#pragma mark HLSelfParser settings

#define    IS_DEBUG             NO
#define    IS_CASE_SENSITIVE    NO