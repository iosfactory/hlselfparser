//
//  HLHelper.h
//  HLSelfParser
//
//  Created by Hervé Droit on 12/07/13.
//  Copyright (c) 2013 Hervé Heurtault de Lammerville. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLHelper : NSObject

+ (NSMutableArray *)getPropertyNamesOfClass :(Class)class;
+ (NSMutableArray *)getFormattedObjectsFromData :(NSData *)data andOfType :(Class)class;

@end
