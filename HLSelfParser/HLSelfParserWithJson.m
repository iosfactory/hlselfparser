//
//  HLSelfParserWithJson.m
//  HLSelfParser
//
//  Created by Hervé Droit on 12/07/13.
//  Copyright (c) 2013 Hervé Heurtault de Lammerville. All rights reserved.
//

#import "HLSelfParserWithJson.h"
#import "HLHelper.h"
#import "HLConstants.h"

@implementation HLSelfParserWithJson

#pragma mark HLSelfParser Requests

+ (void)parseWithHttpURL :(NSURL *)url andClass:(Class)class;
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError *error = nil;
        NSString *json = [NSString stringWithContentsOfURL:url
                                                  encoding:NSUTF8StringEncoding
                                                     error:&error];
        NSData *data = [json dataUsingEncoding:NSUnicodeStringEncoding];
        
        if (error)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"couldNotRetrieveJsonFile" object:error];
            
            if (IS_DEBUG)
                NSLog(@"HLSelfParserWithJson.m <parseWithHttpURL> -- Error received while retrieving JSON file: %@", [error localizedDescription]);
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSMutableArray *objects = [HLHelper getFormattedObjectsFromData:data andOfType:class];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"objectsRetrieved" object:objects];
            });
        }
	});
}

+ (void)parseWithHttpPostURL :(NSURL *)url andParams:(NSDictionary *)parameters andClass:(Class)class
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        NSError *error;
        NSData *requestBody          = [NSJSONSerialization dataWithJSONObject:parameters
                                                                       options:kNilOptions
                                                                         error:&error];
        
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:requestBody];
        
        NSURLResponse *response = nil;
        NSError *requestError = nil;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                     returningResponse:&response
                                                                 error:&requestError];
        
        if (error)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"couldNotRetrieveJsonFile" object:error];
            
            if (IS_DEBUG)
                NSLog(@"HLSelfParserWithJson.m <parseWithHttpPostURL> -- Error received while retrieving JSON file: %@", [error localizedDescription]);
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSMutableArray *objects = [HLHelper getFormattedObjectsFromData:responseData andOfType:class];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"objectsRetrieved" object:objects];
            });
        }
    });
}

+ (void)parseFromLocalString :(NSString *)localString andClass:(Class)class
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError *error;
        NSData *localData = [localString dataUsingEncoding:NSUTF8StringEncoding];
        
        if (error)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"couldNotRetrieveJsonFile" object:error];
            
            if (IS_DEBUG)
                NSLog(@"HLSelfParserWithJson.m <parseFromLocalString> -- Error received while parsing JSON file: %@", [error localizedDescription]);
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSMutableArray *objects = [HLHelper getFormattedObjectsFromData:localData andOfType:class];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"objectsRetrieved" object:objects];
            });
        }
    });
}

@end
