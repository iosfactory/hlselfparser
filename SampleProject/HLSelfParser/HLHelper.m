//
//  HLHelper.m
//  HLSelfParser
//
//  Created by Hervé Droit on 12/07/13.
//  Copyright (c) 2013 Hervé Heurtault de Lammerville. All rights reserved.
//

#import "HLHelper.h"
#import "HLConstants.h"
#import <objc/runtime.h>

@implementation HLHelper

+ (NSMutableArray *)getPropertyNamesOfClass :(Class)class
{
    NSMutableArray *properties_array = [[NSMutableArray alloc] init];
    unsigned int properties_count    = 0;
    objc_property_t *properties      = class_copyPropertyList(class, &properties_count);
    
    for (unsigned int i = 0; i < properties_count; i++)
    {
        objc_property_t property  = properties[i];
        const char *property_name = property_getName(property);
        
        [properties_array addObject:[NSString stringWithUTF8String:property_name]];
    }
    
    if (IS_DEBUG)
    {
        NSLog(@"HLHelper.m <getPropertyNamesOfClass> -- Properties names:");
        NSLog(@"%@", properties_array);
    }
    
    free(properties);
    return properties_array;
}

+ (NSMutableArray *)getFormattedObjectsFromData :(NSData *)data andOfType :(Class)class
{
    NSError *error     = nil;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&error];
    if (error)
    {
        if (IS_DEBUG)
            NSLog(@"HLHelper.m <getFormattedObjectsFromData -- Error while parsing json file: %@", [error localizedDescription]);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"couldNotRetrieveJsonFile" object:error];
        return nil;
    }
    
    
    NSString *className = [[NSString alloc] init];
    className           = NSStringFromClass(class);
    if (!IS_CASE_SENSITIVE)
        className = [className lowercaseString];
    NSArray *source     = [json objectForKey:className];
    
    if (IS_DEBUG)
        NSLog(@"HLHelper.m <getFormattedObjectsFromData -- Objects count for class -%@-: %d", className, [source count]);
    
    NSMutableArray *properties = [HLHelper getPropertyNamesOfClass:class];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (unsigned int i = 0; i < [source count]; i++)
    {
        NSDictionary *currentObject = [source objectAtIndex:i];
        id object = [[class alloc] init];
        
        for (NSString *property in properties)
        {
            [object setValue:[currentObject objectForKey:property] forKey:property];
        }
        
        [result addObject:object];
    }
    
    return result;
}

@end
