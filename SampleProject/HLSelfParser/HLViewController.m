//
//  HLViewController.m
//  HLSelfParser
//
//  Created by Hervé Droit on 12/07/13.
//  Copyright (c) 2013 Hervé Heurtault de Lammerville. All rights reserved.
//

#import "HLViewController.h"
#import "HLSelfParserWithJson.h"
#import "Questions.h"

@interface HLViewController ()

@end

@implementation HLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    result = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(objectsRetrieved:) name:@"objectsRetrieved" object:nil];
    [HLSelfParserWithJson parseWithHttpURL:[NSURL URLWithString:@"http://prod-mib-extern.makeitbad.fr/test.html"] andClass:[Questions class]];
}

- (void)objectsRetrieved :(NSNotification *)sender
{
    result = (NSMutableArray *)(sender.object);
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [result count];
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CarCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    Questions *question = (Questions *)[result objectAtIndex:indexPath.row];
    [cell.textLabel setText:question.q_title];
    [cell.detailTextLabel setText:question.q_b];
    
    return cell;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
}
@end
