//
//  HLSelfParserWithJson.h
//  HLSelfParser
//
//  Created by Hervé Droit on 12/07/13.
//  Copyright (c) 2013 Hervé Heurtault de Lammerville. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HLSelfParserWithJson : NSObject <NSURLConnectionDelegate>

+ (void)parseWithHttpURL :(NSURL *)url andClass:(Class)class;
+ (void)parseWithHttpPostURL :(NSURL *)url andParams:(NSDictionary *)parameters andClass:(Class)class;
+ (void)parseFromLocalString :(NSString *)localString andClass:(Class)class;

@end
