//
//  Questions.h
//  HLSelfParser
//
//  Created by Hervé Droit on 12/07/13.
//  Copyright (c) 2013 Hervé Heurtault de Lammerville. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Questions : NSObject

@property (nonatomic, strong) NSString  *q_title;
@property (nonatomic, strong) NSArray   *q_g;
@property (nonatomic, strong) NSString  *q_b;

@end
