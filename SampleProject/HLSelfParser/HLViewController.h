//
//  HLViewController.h
//  HLSelfParser
//
//  Created by Hervé Droit on 12/07/13.
//  Copyright (c) 2013 Hervé Heurtault de Lammerville. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLViewController : UITableViewController <UITabBarDelegate, UITableViewDataSource>
{
    NSMutableArray *result;
}

@end
